declare module '*.svg';
declare module 'jszip';
declare module 'dragula-with-animation';
declare module '!raw-loader!*';

interface CurrentList<T> {
    current: T | undefined;
    list: T[];
}

interface AdvancedType {
    kind: 'unit' | 'dropdown' | 'dropdownFixed' | 'date' | 'longString' | 'string' | 'number' | 'boolean';
    hidden?: true;
    readonly?: true;
    oldValue?: any;
    value: any;
}
