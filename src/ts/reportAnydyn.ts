import * as Mapbox from 'mapbox-gl'
import { ReportBase } from './reportBase'
import { Unit } from './unit'
import { Threshold } from './threshold'

export class ReportAnydyn extends ReportBase {
  public machine: string = ''

  public bearingCapacityUnitString: string = 'um'

  public forceUnitSting: string = 'kN'

  public qualityUnitSting: string = '%'

  public bearingCapacityName: string

  public thresholds: CurrentList<Threshold> = {
    current: undefined,
    list: []
  }

  public values: CurrentList<string> = {
    current: undefined,
    list: []
  }

  constructor(
    map: Mapbox.Map,
    bearingCapacityName: string,
    bearingCapacityUnitString: string,
    minPortance: number,
    maxPortance: number
  ) {
    super(map, minPortance, maxPortance)

    this.bearingCapacityName = bearingCapacityName
    this.bearingCapacityUnitString = bearingCapacityUnitString

    this.thresholds.list.push(
      ...[
        new Threshold('N.S', this.bearingCapacityUnitString, 0),
        new Threshold('AR1', this.bearingCapacityUnitString, 20),
        new Threshold('AR2', this.bearingCapacityUnitString, 50),
        new Threshold('AR3', this.bearingCapacityUnitString, 120),
        new Threshold('AR4', this.bearingCapacityUnitString, 200),
        new Threshold('PF1', this.bearingCapacityUnitString, 20),
        new Threshold('PF2', this.bearingCapacityUnitString, 50),
        new Threshold('PF2+', this.bearingCapacityUnitString, 80),
        new Threshold('PF3', this.bearingCapacityUnitString, 120),
        new Threshold('PF4', this.bearingCapacityUnitString, 200),
        new Threshold('other', this.bearingCapacityUnitString, 10, 10, 0, 250)
      ]
    )

    this.thresholds.current = this.thresholds.list[0]
  }

  public getAverage(): number {
    if (this.values.current) {
      let average: number = 0
      let count: number = 0

      // eslint-disable-next-line no-restricted-syntax
      for (const POINT of this.points.children) {
        if (!POINT.isHidden() && POINT.values[this.values.current]) {
          count++

          if (this.values.current !== 'Portance') {
            average +=
              POINT.values[this.values.current].kind === 'unit'
                ? POINT.values[this.values.current].value.toNumber() || 0
                : POINT.values[this.values.current].value || 0
          } else {
            const value = POINT.values[this.values.current].value.toNumber()

            average +=
              value < this.minPortance
                ? this.minPortance
                : value > this.maxPortance
                ? this.maxPortance
                : value
          }
        }
      }

      return count > 0 ? average / count : 0
    } else {
      return 0
    }
  }
}
