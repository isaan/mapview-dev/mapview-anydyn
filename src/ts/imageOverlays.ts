import * as Mapbox from 'mapbox-gl';
import { ImageOverlay } from './imageOverlay';

export class ImageOverlays {
    private children: ImageOverlay[] = [];

    public static addImageOverlay(map: Mapbox.Map, imageOverlay: ImageOverlay): void {
        // write code here
    }

    public static clean(map: Mapbox.Map): void {
        // write code here
    }

    public static hide(state: boolean): void {
        // write code here
    }

    public static init(map: Mapbox.Map): void {
        // write code here
    }
}
