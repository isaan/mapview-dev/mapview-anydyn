TABLE Database
Logiciel	Anydyn
VersionBDD	3
TABLE Dossiers
Nom	Demo Minidyn
Client	Rincent NDT
Dossier	21-1258
Localite	Bordeaux
Date	09/03/2020
Ouvrage	Parc de Bercy
Commentaire	
TABLE PVs
Nom	PV01
Date	09/03/2020
Operateur	Sébastien Pellevrault
Voie	11 Quai de Conti PARIS
Sens	Gauche
PartieOuvrage Voie Vélo	
Climat	Enseillé
Temperature	12
Commentaire	
TABLE ResultatsPV
Points_Nombre	6
PointsOK_Nombre	6
PointsOK_Max	74,6
PointsOK_Min	38,1
PointsOK_Moyenne	52,1
PointsOK_Std	16
PointsKOMin_Nombre	0
PointsKOMax_Nombre	0
PointsPass_Nombre	6
TABLE Plateformes
Type	Voirie
Support	
Couche	Couche de forme
Materiau	Sablon
GTR	N.S
Classe	PF2
Seuil	50,00
TABLE ParamsPortance
Nom	Ecor Exp.
AlgoRaideur	Max
AlgoProcessing1	Boussinesq
AlgoProcessing2	Cor. exponentielle
DPlaque	0,3
CPoisson	0,35
FForme	4
K	2,34
Alpha	0,86
MinPortance	10
MaxPortance	110
TABLE TypesResultatPortance
Nom	Module (MPa)
ResultStr	Module
ResultUnitStr	MPa
TABLE ParamsPoint
NbMiseEnPlace	1
NbMoyennage	2
NbTotal	3
TABLE Machines
Serial	MIN-117E
Attribution	Rincent NDT
MAC	A0-B4-14-A7-84-A8-74-36
LicStart	06/12/2019
LicEnd	13/08/2033
CertStart	06/12/2019
CertEnd	26/12/2020
TABLE ParamsAcqu
NbSamples	1500
FreqAcqu	30000,00
PreTrig	0,01
TABLE ReportPoints
Numero	1
Portance	40,9
Qualite	NaN
Latitude	48,856060
Longitude	2,338203
DOP	1,0
Chainage	0
Commentaire	
Date	09/03/2021 12:07
TABLE Frappes
Numero	0
Portance	39,2
Qualite	NaN
Fmax	7,2
Umax	793,2
PulseTime	14,6
TABLE Frappes
Numero	1
Portance	43,4
Qualite	NaN
Fmax	8,2
Umax	805,4
PulseTime	13,7
TABLE Frappes
Numero	2
Portance	38,5
Qualite	NaN
Fmax	7,5
Umax	849,0
PulseTime	15,1
TABLE ReportPoints
Numero	2
Portance	45,3
Qualite	NaN
Latitude	48,856117
Longitude	2,338184
DOP	1,0
Chainage	0
Commentaire	
Date	09/03/2021 12:08
TABLE Frappes
Numero	0
Portance	42,5
Qualite	NaN
Fmax	7,6
Umax	761,3
PulseTime	14,7
TABLE Frappes
Numero	1
Portance	44,3
Qualite	NaN
Fmax	7,6
Umax	725,1
PulseTime	15,0
TABLE Frappes
Numero	2
Portance	46,3
Qualite	NaN
Fmax	7,6
Umax	690,1
PulseTime	14,6
TABLE ReportPoints
Numero	3
Portance	38,1
Qualite	NaN
Latitude	48,856069
Longitude	2,338264
DOP	1,0
Chainage	0
Commentaire	
Date	09/03/2021 12:14
TABLE Frappes
Numero	0
Portance	37,3
Qualite	NaN
Fmax	7,5
Umax	876,3
PulseTime	14,0
TABLE Frappes
Numero	1
Portance	36,5
Qualite	NaN
Fmax	7,7
Umax	917,7
PulseTime	15,1
TABLE Frappes
Numero	2
Portance	39,7
Qualite	NaN
Fmax	7,8
Umax	851,6
PulseTime	15,0
TABLE ReportPoints
Numero	4
Portance	74,6
Qualite	NaN
Latitude	48,856139
Longitude	2,338316
DOP	1,0
Chainage	0
Commentaire	
Date	09/03/2021 12:14
TABLE Frappes
Numero	0
Portance	53,1
Qualite	NaN
Fmax	7,8
Umax	605,5
PulseTime	14,8
TABLE Frappes
Numero	1
Portance	71,9
Qualite	NaN
Fmax	7,9
Umax	427,9
PulseTime	14,6
TABLE Frappes
Numero	2
Portance	77,3
Qualite	NaN
Fmax	7,7
Umax	388,1
PulseTime	14,4
TABLE ReportPoints
Numero	5
Portance	43,3
Qualite	NaN
Latitude	48,856085
Longitude	2,338338
DOP	1,0
Chainage	0
Commentaire	
Date	09/03/2021 12:15
TABLE Frappes
Numero	0
Portance	19,3
Qualite	NaN
Fmax	7,3
Umax	1839,5
PulseTime	15,6
TABLE Frappes
Numero	1
Portance	39,5
Qualite	NaN
Fmax	7,8
Umax	849,0
PulseTime	15,0
TABLE Frappes
Numero	2
Portance	47,1
Qualite	NaN
Fmax	7,8
Umax	697,9
PulseTime	15,1
TABLE ReportPoints
Numero	6
Portance	70,6
Qualite	NaN
Latitude	48,855995
Longitude	2,338373
DOP	1,0
Chainage	0
Commentaire	
Date	09/03/2021 12:17
TABLE Frappes
Numero	0
Portance	24,1
Qualite	NaN
Fmax	8,0
Umax	1556,4
PulseTime	15,1
TABLE Frappes
Numero	1
Portance	78,4
Qualite	NaN
Fmax	7,9
Umax	387,1
PulseTime	14,9
TABLE Frappes
Numero	2
Portance	62,8
Qualite	NaN
Fmax	7,9
Umax	507,4
PulseTime	14,7
	
